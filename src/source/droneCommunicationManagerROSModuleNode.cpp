/*
*
*
*
*
*/

// ROS
#include "ros/ros.h"

// C++ standar library
#include <stdio.h>
#include <iostream>

#include "droneCommunicationManagerROSModule.h"

#include "nodes_definition.h"


using namespace std;

int main(int argc, char **argv)
{
    ros::init(argc, argv, MODULE_NAME_ARCHITECTURE_BRAIN);
    ros::NodeHandle n;

    //Init
    cout<<"Starting "<<MODULE_NAME_ARCHITECTURE_BRAIN<<endl;

    //MyDroneCommunicationManagerROSModule
    DroneCommunicationManagerROSModule MyDroneCommunicationManagerROSModule;

    // Open
    MyDroneCommunicationManagerROSModule.open(n,MODULE_NAME_ARCHITECTURE_BRAIN);


    // Start
    MyDroneCommunicationManagerROSModule.start();



    //Loop
    while(ros::ok()) 
    {
        ros::spinOnce();
        if(!MyDroneCommunicationManagerROSModule.run())
        {

        }

        MyDroneCommunicationManagerROSModule.sleep();
    }


    return 1;
}
