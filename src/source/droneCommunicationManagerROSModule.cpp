#include "droneCommunicationManagerROSModule.h"

DroneCommunicationManagerROSModule::DroneCommunicationManagerROSModule()
    : DroneModule( droneModule::active, FREQ_DRONE_COMMUNICATION_MANAGER_ROS_MODULE)
    , droneIsInTheSystem(false)
{
    init();
    societyMembers.clear();
    societyIds.clear();
    return;
}

DroneCommunicationManagerROSModule::~DroneCommunicationManagerROSModule()
{
    close();
    return;
}

void DroneCommunicationManagerROSModule::init()
{
    //end
    DroneModule::init();
    return;
}

void DroneCommunicationManagerROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);



    // Get Parameters
    //
    ros::param::get("~mission_state_topic_name", missionStateTopicName);
    if ( missionStateTopicName.length() == 0)
    {
        missionStateTopicName="missionState";
    }
    std::cout<<"mission_state_topic_name="<<missionStateTopicName<<std::endl;

    //
    ros::param::get("~is_in_the_system_topic_name", isInTheSystemTopicName);
    if ( isInTheSystemTopicName.length() == 0)
    {
        isInTheSystemTopicName="isInTheSystem";
    }
    std::cout<<"is_in_the_system_topic_name="<<isInTheSystemTopicName<<std::endl;

    //
    ros::param::get("~society_broadcast_topic_name", societyBroadcastTopicName);
    if ( societyBroadcastTopicName.length() == 0)
    {
        societyBroadcastTopicName="/societyBroadcast";
    }
    std::cout<<"society_broadcast_topic_name="<<societyBroadcastTopicName<<std::endl;

    //
    ros::param::get("~society_pose_topic_name", societyPoseTopicName);
    if ( societyPoseTopicName.length() == 0)
    {
        societyPoseTopicName="societyPose";
    }
    std::cout<<"society_pose_topic_name="<<societyPoseTopicName<<std::endl;


    // Subscribers and publishers
    missionStateSub = n.subscribe(missionStateTopicName, 1, &DroneCommunicationManagerROSModule::missionStateCallback, this);

    isInTheSystemPubl    = n.advertise<std_msgs::Bool>( isInTheSystemTopicName, 1, true);

    societyBroadcastPubl = n.advertise<std_msgs::Int32>( societyBroadcastTopicName, 1, true);
    societyBroadcastSubs = n.subscribe( societyBroadcastTopicName, 100, &DroneCommunicationManagerROSModule::societyBroadcastCallback, this);

    societyPosePubl      = n.advertise<droneMsgsROS::societyPose>( societyPoseTopicName, 1, true);


    //Flag of module opened
    droneModuleOpened=true;

    return;
}

void DroneCommunicationManagerROSModule::close()
{
    DroneModule::close();
    return;
}

bool DroneCommunicationManagerROSModule::run()
{
    DroneModule::run();

    // publish this_drone's id number in the society broadcast
    std_msgs::Int32 my_id;
    my_id.data = idDrone;
    societyBroadcastPubl.publish(my_id);

    // Publish drone is in the system
    std_msgs::Bool this_drone_is_in_the_system;
    this_drone_is_in_the_system.data = droneIsInTheSystem;
    isInTheSystemPubl.publish(this_drone_is_in_the_system);

    // Publish society pose
    publishSocietyPose();

    return true;
}

void DroneCommunicationManagerROSModule::societyBroadcastCallback(const std_msgs::Int32::ConstPtr &msg)
{
    // msg->data has the other drone's idDrone parameter
    if (msg->data == idDrone)
    {
        return;
    }

    bool not_found = true;

    for (unsigned int i=0; i<societyIds.size(); i++)
    {
        if ( societyIds[i] == (msg->data) )
        {
            not_found = false;
        }
    }

    if ( not_found ) // msg->data not in societyIds
    {
        societyIds.push_back(msg->data);

        societyMembers.push_back( OtherSwarmAgentListener(msg->data) );

        societyMembers.back().open(n);
    }
    return;
}

void DroneCommunicationManagerROSModule::missionStateCallback(const std_msgs::Bool::ConstPtr & msg)
{
    droneIsInTheSystem=msg->data;

    return;
}

void DroneCommunicationManagerROSModule::publishSocietyPose()
{
    societyPose_msg.societyDrone.clear();

    for (std::list<OtherSwarmAgentListener>::iterator it = societyMembers.begin();
         it != societyMembers.end();
         ++it)
    {
        if (it->isInTheSystem()) // equivalente a isOnline() and isStarted()
        {
            societyPose_msg.societyDrone.push_back( it->getDroneInfo() );
        }
    }

    societyPosePubl.publish(societyPose_msg);
}
