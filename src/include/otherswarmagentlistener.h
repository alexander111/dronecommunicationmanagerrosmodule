#ifndef OTHERSWARMAGENTLISTENER_H
#define OTHERSWARMAGENTLISTENER_H

// ROS
#include "ros/ros.h"

// C++ standar libraries
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <string>

#include "std_msgs/Bool.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneInfo.h"

#define OTHER_SWARM_AGENT_LAN_TIMEOUT_THRESHOLD (2.0)

class OtherSwarmAgentListener
{

public:
    OtherSwarmAgentListener(int idDrone_in);
    ~OtherSwarmAgentListener();

public:
    void open(ros::NodeHandle & nIn);


protected:
    ros::NodeHandle n;
public:
    int idDrone;


public:
    droneMsgsROS::droneInfo getDroneInfo();
    droneMsgsROS::dronePose getEstimatedPose();
    bool isOnline();
    bool isInTheSystem();


private:
    std::string isInTheSystemTopicName;
    ros::Subscriber     isInTheSystemSub;
    void isOnlineSubCallback(const std_msgs::Bool::ConstPtr &msg);

protected:
    bool                swarmAgentIsOnline;
    ros::Time           last_isOnline_timestamp;
    bool                last_isInTheSystem;



protected:
    std::string estimatedPoseTopicName;
    ros::Subscriber      estimatedPoseSub;
    void estPoseSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg);

public:
    droneMsgsROS::droneInfo last_droneInfo;
private:
    ros::Time            last_estimatedPose_timestamp;
};

#endif // OTHERSWARMAGENTLISTENER_H
