#ifndef DRONE_COMMUNICATION_MANAGER_ROS_MODULE_H
#define DRONE_COMMUNICATION_MANAGER_ROS_MODULE_H

// ROS
#include "ros/ros.h"

// C++ standar libraries
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <set>
#include <algorithm>
#include <sstream>
#include <string>

// DroneModule parent class
#include "droneModuleROS.h"

// droneMsgsROS
#include "droneMsgsROS/droneNavData.h"
#include "droneMsgsROS/societyPose.h"
#include "droneMsgsROS/droneInfo.h"

// Messages
#include "std_msgs/Int32.h"


#include "otherswarmagentlistener.h"


#define FREQ_DRONE_COMMUNICATION_MANAGER_ROS_MODULE  20.0


class DroneCommunicationManagerROSModule : public DroneModule
{

public:
    DroneCommunicationManagerROSModule();
    ~DroneCommunicationManagerROSModule();

public:
    void init();
    void open(ros::NodeHandle & nIn, std::string moduleName);
    void close();
    bool run();


protected:
    bool droneIsInTheSystem;
    std::list<OtherSwarmAgentListener> societyMembers;
    std::vector<int> societyIds;



    // Mission State
protected:
    std::string missionStateTopicName;
    ros::Subscriber missionStateSub;
    void missionStateCallback(const std_msgs::Bool::ConstPtr & msg);


protected:
    std::string isInTheSystemTopicName;
    ros::Publisher  isInTheSystemPubl; // publishes whether the drone has to be considered as an obstacle by other drones


    // "/societyBroadcast"
protected:
    std::string societyBroadcastTopicName;

protected:
    ros::Publisher  societyBroadcastPubl; // publishes this->idDrone

protected:
    ros::Subscriber societyBroadcastSubs; // subscription, what other drones are there in the swarm?
    void societyBroadcastCallback(const std_msgs::Int32::ConstPtr &msg);



    // "societyPose" publishing
protected:
    std::string societyPoseTopicName;
    ros::Publisher  societyPosePubl;
    droneMsgsROS::societyPose societyPose_msg;
    void publishSocietyPose();





};
#endif // DRONE_COMMUNICATION_MANAGER_ROS_MODULE_H
