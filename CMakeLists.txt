cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME droneCommunicationManagerROSModule)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
add_definitions(-std=c++11)


# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries



set(DRONE_COMMUNICATION_MANAGER_ROS_MODULE_SOURCE_DIR
        src/source)

set(DRONE_COMMUNICATION_MANAGER_ROS_MODULE_INCLUDE_DIR
        src/include)

set(DRONE_COMMUNICATION_MANAGER_ROS_MODULE_HEADER_FILES
        ${DRONE_COMMUNICATION_MANAGER_ROS_MODULE_INCLUDE_DIR}/droneCommunicationManagerROSModule.h
        ${DRONE_COMMUNICATION_MANAGER_ROS_MODULE_INCLUDE_DIR}/otherswarmagentlistener.h
)

set(DRONE_COMMUNICATION_MANAGER_ROS_MODULE_SOURCE_FILES
        ${DRONE_COMMUNICATION_MANAGER_ROS_MODULE_SOURCE_DIR}/droneCommunicationManagerROSModule.cpp
        ${DRONE_COMMUNICATION_MANAGER_ROS_MODULE_SOURCE_DIR}/otherswarmagentlistener.cpp
)


find_package(catkin REQUIRED
                COMPONENTS roscpp std_msgs droneMsgsROS droneModuleROS)


catkin_package(
	INCLUDE_DIRS ${DRONE_COMMUNICATION_MANAGER_ROS_MODULE_INCLUDE_DIR}
        CATKIN_DEPENDS roscpp std_msgs droneMsgsROS droneModuleROS
  )


include_directories(${DRONE_COMMUNICATION_MANAGER_ROS_MODULE_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})




add_library(droneCommunicationManagerROSModule ${DRONE_COMMUNICATION_MANAGER_ROS_MODULE_SOURCE_FILES} ${DRONE_COMMUNICATION_MANAGER_ROS_MODULE_HEADER_FILES})
add_dependencies(droneCommunicationManagerROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneCommunicationManagerROSModule ${catkin_LIBRARIES})



add_executable(droneCommunicationManagerROSModuleNode ${DRONE_COMMUNICATION_MANAGER_ROS_MODULE_SOURCE_DIR}/droneCommunicationManagerROSModuleNode.cpp)
add_dependencies(droneCommunicationManagerROSModuleNode ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneCommunicationManagerROSModuleNode droneCommunicationManagerROSModule)
target_link_libraries(droneCommunicationManagerROSModuleNode ${catkin_LIBRARIES})

